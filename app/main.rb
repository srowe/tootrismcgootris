$gtk.reset

module Tetris
  BOX = 30
  Colors = {
    blue: [0,0,255],
    choco: [100,25,55],
    cyan: [0,255,255],
    green: [0,255,0],
    grey: [127,127,127],
    purple: [255,0,255],
    red: [255,0,0],
    yellow: [255,255,0],
  }
  GridHeight = 20
  GridWidth  = 10
  PIXEL_WIDTH = 1280
  PIXEL_HEIGHT = 720

  OFFSET_X = (PIXEL_WIDTH - (GridWidth * BOX)) / 2
  OFFSET_Y = (PIXEL_HEIGHT - ((GridHeight - 2) * BOX)) / 2
  TurnDuration = 30

  class Piece
    Shapes = [
      {color: :blue, shape: [ [ 0, 1, 1 ], [ 1, 1, 0 ] ]},
      {color: :choco , shape: [ [ 1, 0, 0 ], [ 1, 1, 1 ] ]},
      {color: :cyan, shape: [ [ 0, 1], [ 1, 1 ], [ 0, 1 ] ]},
      {color: :green, shape: [[1],[1],[1],[1]]},
      {color: :purple, shape: [ [ 1, 1], [ 1, 1 ] ]},
      {color: :red, shape: [ [ 0, 0, 1], [ 1, 1, 1 ] ]},
      {color: :yellow, shape: [ [ 1, 1, 0 ], [ 0, 1, 1 ] ]},
    ]
    def self.random
      self.new(Shapes.sample)
    end
    def initialize(obj)
      @color = obj[:color]
      @me    = obj[:shape]
      coords_for_next!
    end
    def bottomed_out?(matrix)
      occupies.any?{|(x,y)| y == GridHeight - 1 || matrix[y+1][x] != :empty }
    end
    def color
      @color
    end
    def coords_for_grid!
      @x = 5
      @y = 0
    end
    def coords_for_next!
      @x = (8 - width)./(2).+(14)
      @y = (8 - height)./(2).+(2)
    end
    def drop!
      @y += 1
    end
    def height
      @me.length
    end
    def occupies
      @me.each_with_index.map do |row,y|
        row.each_with_index.map do |point,x|
          if point == 0
            nil
          else
            [@x + x - 1, @y + y]
          end
        end.compact
      end.flatten(1)
    end
    def rotate_left
      @me = @me.transpose.map(&:reverse)
      @x = GridWidth - width if @x + width >= GridWidth
    end
    def rotate_right
      @me = @me.transpose.map(&:reverse)
      @me = @me.transpose.map(&:reverse)
      @me = @me.transpose.map(&:reverse)
      @x = GridWidth - width if @x + width >= GridWidth
    end
    def to_a
      @me
    end
    def width
      @me.first.length
    end
    def x_move(n,matrix)
      newval = @x + n
      return if newval <= 0 || newval + width >= GridWidth.+(2) # wall collision
      return if occupies.any?{|(ox,oy)| matrix[oy][ox+n] != :empty} # side-to-side collision
      @x = newval
    end
    def debug
      {
        height: height,
        me: @me,
        occupies: occupies,
        width: width,
        x: @x,
        y: @y,
      }
    end
  end

  class Grid
    def initialize(args)
      @args      = args
      @game_over = false
      @matrix    = Array.new(GridHeight){ row! }
      @next_move = TurnDuration
      @score     = 0
      next_piece
      next_piece
    end
    def bottomed_out?
      current_piece.bottomed_out?(matrix)
    end
    def current_piece
      @current_piece
    end
    def handle_input
      if @game_over
        $gtk.reset if keyboard.key_down.space
        return
      end
      $gtk.reset if keyboard.key_down.r
      #$gtk.paused = ! $gtk.paused if keyboard.key_down.p
      current_piece.x_move(-1,matrix) if keyboard.key_down.left
      current_piece.x_move(+1,matrix) if keyboard.key_down.right
      current_piece.rotate_left if keyboard.key_down.a
      current_piece.rotate_right if keyboard.key_down.b
      @next_move -= 10 if keyboard.key_down.down || keyboard.key_held.down

      @next_move -= 1
      if @next_move <= 0
        if bottomed_out?
          plant!
        else
          current_piece.drop!
        end
        @next_move = TurnDuration
      end
    end
    def keyboard
      @args.inputs.keyboard
    end
    def matrix
      @matrix
    end
    def next_piece
      @next_piece.coords_for_grid!
      @current_piece = @next_piece
      @next_piece = Piece.random
    end
    def plant!
      return unless bottomed_out?
      current_piece.occupies.each do |row|
        x,y = *row
        @matrix[y][x] = current_piece.color
      end
      score!
      next_piece
      @game_over = bottomed_out?
    end
    def render
      matrix.each_with_index do |row,grid_x|
        row.each_with_index do |color,grid_y|
          render_cube(grid_y,grid_x,color) unless color == :empty
        end
      end
    end
    def render_background
      @args.outputs.sprites << [ 75, 300, 300, 300, 'console-logo.png' ]
      @args.outputs.solids << [ 0, 0, 1280, 720, 0, 0, 0 ]
      render_border(-1, -1, GridWidth + 2, GridHeight + 2)
    end
    def render_border(x,y,width,height)
      for i in x..(x+width)-1 do
        render_cube(i, y, :grey)
        render_cube(i, (y+height)-1, :grey)
      end
      for i in y..(y+height)-1 do
        render_cube(x, i, :grey)
        render_cube((x+width)-1, i, :grey)
      end
    end
    def render_cube(x,y,color)
      @args.outputs.solids << [ OFFSET_X + (x * BOX), (PIXEL_HEIGHT - OFFSET_Y) - (y * BOX), BOX, BOX, *Colors[color] ]
      @args.outputs.borders << [ OFFSET_X + (x * BOX), (PIXEL_HEIGHT - OFFSET_Y) - (y * BOX), BOX, BOX, 255, 255, 255, 255 ]
    end
    def render_current_piece
      render_piece(current_piece)
    end
    def render_next_piece
      render_border(13, 2, 8, 8)
      render_piece(@next_piece)
      @args.outputs.labels << [ 910, 640, "Next piece", 10, 255, 255, 255, 255 ]
    end
    def render_piece(piece)
      piece.occupies.each do |row|
        x,y = *row
        render_cube(x,y,piece.color)
      end
    end
    def render_score
      @args.outputs.labels << [ 10, 30, @args.gtk.current_framerate, 10, 244, 244, 244]
      @args.outputs.labels << [ 75, 75, "Score: #{@score}", 10, 255, 255, 255, 255 ]
      @args.outputs.labels << [ 200, 450, "GAME OVER", 100, 255, 255, 255, 255 ] if @game_over
    end
    def row!
      Array.new(GridWidth){ :empty }
    end
    def score!
      future_matrix = matrix.reject{|row| row.all?{|ele| ele != :empty }}
      delta = @matrix.length - future_matrix.length
      if delta > 0
        delta.times do
          @score += 1
          future_matrix.unshift(row!)
        end
        @matrix = future_matrix
      end
      @score += 1 if delta == 4 # give a bonus for a full-rack
    end
    def tick
      handle_input
      render_background
      render
      render_next_piece
      render_current_piece
      render_score
    end
  end
end

def tick args
  args.state.game ||= Tetris::Grid.new(args)
  args.state.game.tick
end
