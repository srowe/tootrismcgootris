# Tootris McGootris

This is my attempt at converting rcgordon's DragonRuby Tetris to a more OO/idiomatic-Ruby style.

Original is in this repository as `original.rb` and was obtained from https://gist.githubusercontent.com/rcgordon/5ff71a692884a601e5554465e2ff69a7/raw/bac8ad0a64332834c9025e86a5b9b4e5383c266c/main.rb

Bugs fixed:

1.  piece side-collisions are now detected
1.  The grid is no longer rotated -90*

Known bugs:

1. It's still possible to rotate a piece into another piece

Silly things added:

1. You get a bonus for a 4-banger
1. You can see the framerate

Roadmap:

1. decouple the `Piece` class from the `Grid` class
1. introduce methods for `@next_move` mutation
1. generally wrap instance variables with methods
1. Speed the game up as the score goes up
1. implement pause
1. Wish that I had `pp`
1. replace the grid with something smarter
1. generally improve the code quality
1. improve method names
1. make `render_cube` more readable and perhaps move it into a module
1. have pieces render themselves
1. get better at DragonRuby
